import { Component, OnInit } from '@angular/core';
import { QuestionnaireSelectors } from '../core/store/questionnaire/questionnaire.selectors';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss']
})
export class AnswersComponent implements OnInit {

  listQuestionnaireAll$ = this.questionnaireSelectors.getQuestionnaireAll();

  constructor(private questionnaireSelectors: QuestionnaireSelectors) { }

  ngOnInit(): void {
  }

}
