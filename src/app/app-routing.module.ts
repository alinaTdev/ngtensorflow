import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AnswersGuard} from './core/guards/answers.guard';
import {AnswersComponent} from './answers/answers.component';
import {QuestionnaireComponent} from './questionnaire/questionnaire.component';
import {UserFormComponent} from './user-form/user-form.component';
import {QuestionnaireGuard} from './core/guards/questionnaire.guard';

const routes: Routes = [
  {path: '', component: UserFormComponent},
  {path: 'questionnaire', component: QuestionnaireComponent, canActivate: [QuestionnaireGuard]},
  {path: 'answers', pathMatch: 'full', component: AnswersComponent, canActivate: [AnswersGuard]},
  {path: '**', redirectTo: ''},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

