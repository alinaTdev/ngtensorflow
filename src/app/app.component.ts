import {Component, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-tensorflow';

  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    // add custom svg to material ico
    this.matIconRegistry.addSvgIcon('wave-hand', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gestures/wave-hand.svg'));
    this.matIconRegistry.addSvgIcon('thumbs-up', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gestures/thumb-up.svg'));
    this.matIconRegistry.addSvgIcon('fist', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gestures/fist.svg'));
    this.matIconRegistry.addSvgIcon('one-finger', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gestures/one-finger.svg'));
    this.matIconRegistry.addSvgIcon('two-fingers', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gestures/two-fingers.svg'));
    this.matIconRegistry.addSvgIcon(
      'rock-fingers', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gestures/rock-fingers.svg'));
    this.matIconRegistry.addSvgIcon('example-close', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/example-close.svg'));
    this.matIconRegistry.addSvgIcon('example-play', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/example-play.svg'));
  }

  ngOnInit(): void {
  }
}


