import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

//#region Material
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';

const Material = [MatButtonModule, MatProgressSpinnerModule, MatSidenavModule, MatCardModule, MatIconModule, MatListModule];
//#endregion

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HeaderComponent} from './header/header.component';
import {QuestionnaireComponent} from './questionnaire/questionnaire.component';
import {InfoComponent} from './questionnaire/info/info.component';
import {ContentComponent} from './questionnaire/content/content.component';
import {ExampleComponent} from './questionnaire/info/example/example.component';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {EffectsModule} from '@ngrx/effects';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {reducers, metaReducers} from './core';
import {QuestionnaireEffects} from './core/store/questionnaire/questionnaire.effects';
import {QuestionnaireSelectors} from './core/store/questionnaire/questionnaire.selectors';
import {QuestionnaireActions} from './core/store/questionnaire/questionnaire.actions';
import {CardComponent} from './questionnaire/content/card/card.component';
import {HttpClientModule} from '@angular/common/http';
import {CardPipe} from './questionnaire/content/card/card.pipe';
import {ListComponent} from './questionnaire/info/example/list/list.component';
import {PreviewComponent} from './questionnaire/info/example/preview/preview.component';
import {VariableDirective} from './questionnaire/info/variable.directive';
import {AnswersComponent} from './answers/answers.component';
import {ButtonsComponent} from './questionnaire/content/buttons/buttons.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {UserFormComponent} from './user-form/user-form.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    QuestionnaireComponent,
    InfoComponent,
    ContentComponent,
    ExampleComponent,
    CardComponent,
    CardPipe,
    ListComponent,
    PreviewComponent,
    VariableDirective,
    AnswersComponent,
    ButtonsComponent,
    UserFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence({synchronizeTabs: true}),
    Material,
    FlexLayoutModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    EffectsModule.forRoot([QuestionnaireEffects]),
    StoreRouterConnectingModule.forRoot(),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
  ],
  exports: [],
  providers: [
    QuestionnaireSelectors,
    QuestionnaireActions,
    AngularFireModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
