import {IGesturesExample} from '../interfaces/gesture';

export const GESTURE_LIST: Array<IGesturesExample> = [
  {
    ico: 'thumbs-up',
    name: 'Agree',
    description: 'Thumbs up',
    gif: 'confirm.gif'
  },
  {
    ico: 'wave-hand',
    name: 'Back',
    description: 'Right hand wave',
    gif: 'wave-hand-back.gif'
  },
  {
    ico: 'wave-hand',
    name: 'Next',
    description: 'Left hand wave',
    gif: 'wave-hand-next.gif'
  },
  {
    ico: 'fist',
    name: 'Disagree',
    description: 'Fist',
    gif: 'fist.gif'
  },
];

