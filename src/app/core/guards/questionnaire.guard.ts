import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {QuestionnaireSelectors} from '../store/questionnaire/questionnaire.selectors';

@Injectable({
  providedIn: 'root'
})
export class QuestionnaireGuard implements CanActivate {

  constructor(private questionnaireSelectors: QuestionnaireSelectors, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const permisson =
      this.questionnaireSelectors.getUserFilledDataStatus()
        .pipe(tap(status => (!status && this.router.navigate(['/']))));
    return permisson;
  }
}
