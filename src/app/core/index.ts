import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as questionnaire from './store/questionnaire/questionnaire.reducer';

export interface State {
  [questionnaire.questionnaireFeatureKey]: questionnaire.State;
}

export const reducers: ActionReducerMap<State> = {
  [questionnaire.questionnaireFeatureKey]: questionnaire.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
