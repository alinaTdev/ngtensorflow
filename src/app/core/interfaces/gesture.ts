import {TGesturesIco} from '../services/hand-gesture.service';

export interface IGesturesExample {
  ico: TGesturesIco;
  name: string;
  description: string;
  gif?: string;
}
