import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IQuestionnaire} from '../store/questionnaire/questionnaire.reducer';
import {AngularFirestore} from '@angular/fire/firestore';

interface IRequestAnswersQuestionnairePartial extends Partial<IQuestionnaire> {
  respondent_answers: Array<IRespondentAnswer>;
  respondent_first_name: string;
  respondent_last_name: string;
  respondent_age: string;
}

export interface IRespondentAnswer {
  question_text: string;
  answer: string;
}

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private firestore: AngularFirestore) {
  }

  getQuestionnaireCards(): Observable<Array<IQuestionnaire>> {
    return new Observable<Array<IQuestionnaire>>((subscriber) => {
      setTimeout(() => {
        subscriber.next([
          {
            id: this.randomId(),
            image: 'assets/svg/card_family.svg',
            text: 'Blandit cursus risus at ultrices. Sed enim ut sem viverra aliquet eget sit amet tellus.'
          },
          {
            id: this.randomId(),
            image: 'https://a1.amlimg.com/MDMzOWJjMDJkNzM4YmYyMTFlZjVkMjZiNTRmYTBkYTBSgeQ0LBRWuU-of_3yPFvhaHR0cDovL21lZGlhLmFkc2ltZy5jb20vN2I1NmFhYTNiNjUzMWQ4Yzg0MGFiMzQyMWQxYWQ4NWMxZWFlMTczNzcwMDdhNDFkNTc1ZTNjZTRiNjg2MDI5ZS5qcGd8ODAwfHx8fHx8aHR0cDovL3d3dy5hZHZlcnRzLmllL3N0YXRpYy9pL3dhdGVybWFyay5wbmd8fGh0dHA6Ly9zdGF0aWMuYW1saW1nLmNvbS9lbXB0eS1pbWFnZXMvbm9uZV8zOTQuanBnfA==.jpg',
            text: 'Sed enim ut sem viverra aliquet eget sit amet tellus. risus at ultrices. Sed enimeget sit amet tellus ut sem viverra aliquet'
          },
          {
            id: this.randomId(),
            image: 'https://cdn.vox-cdn.com/thumbor/bELn1LSbYYmm37rlUSm6Xs7QCIU=/0x0:2040x1360/1200x800/filters:focal(857x517:1183x843)/cdn.vox-cdn.com/uploads/chorus_image/image/69194903/DSCF1179.0.0.jpg',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque illum iusto sed deserunt unde laudantium in deleniti? Tempore a ullam vel quasi dicta?  Optio maiores molestiae at libero tenetur mollitia.'
          },
          {
            id: this.randomId(),
            image: 'https://cdn.motor1.com/images/mgl/lggY1/s1/nissan-qashqai-2021.webp',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque illum iusto sed deserunt unde laudantium in deleniti?'
          },
          {
            id: this.randomId(),
            image: 'https://mediaim.expedia.com/localexpert/522372/bea22eec-b70c-4430-85fd-9bea8008f638.jpg?impolicy=resizecrop&rw=1005&rh=565',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore a ullam vel quasi dicta?'
          },
          {
            id: this.randomId(),
            image: 'https://kharkivskiy-avtocentr.ukravto.ua/files/111%20kia.jpg',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque illum iusto sed deserunt unde dicta'
          },
          {
            id: this.randomId(),
            image: 'https://polarstar-nautical.ru/uploads/articles/26/1_1.jpg',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque illum iusto sed deserunt unde laudantium in deleniti? Tempore a ullam vel quasi dicta?'
          },
          {
            id: this.randomId(),
            image: 'https://www.schengenvisas.com/wp-content/uploads/2020/12/Do-U.S.-citizens-and-green-card-holders-need-a-visa-to-visit-Schengen-area-scaled.jpg',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque illum iusto sed deserunt unde laudantium in deleniti? Tempore a ullam vel quasi dicta?'
          },
          {
            id: this.randomId(),
            image: 'https://i.ytimg.com/vi/1Ne1hqOXKKI/maxresdefault.jpg',
            text: 'Lorem ipsum dolor sit, amet consectetur. Cumque illum iusto sed deserunt unde laudantium in deleniti? Tempore a ullam vel quasi dicta?'
          },
          {
            id: this.randomId(),
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=1.00xw:0.669xh;0,0.190xh&resize=1200:*',
            text: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cumque illum in deleniti? Tempore a ullam vel quasi dicta?'
          }
        ]);
      }, 100);
      return subscriber;
    });
  }

  sendAnswersQuestionnaire(data: IRequestAnswersQuestionnairePartial): any {
    return this.firestore
      .collection('answer-list')
      .add(data).then(res => {
        return ({success: true});
      });
  }

  private randomId(): string {
    const min = 1000;
    const max = 9999;
    return (Math.floor(Math.random() * (max - min + 1)) + min).toString();
  }
}
