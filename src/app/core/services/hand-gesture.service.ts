import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import * as handpose from '@tensorflow-models/handpose';

import '@tensorflow/tfjs-backend-webgl';
import '@tensorflow/tfjs-backend-cpu';
import {drawKeypoints} from '../../hand-renderer';
import {GE} from '../../fingere-gesture';
import {Coords3D} from '@tensorflow-models/handpose/dist/pipeline';
import {QuestionnaireActions} from '../store/questionnaire/questionnaire.actions';

/** Render callbacks */
type IRunAnimationCallback = (data: IRunAnimationCallbackStart) => void;

/** Callback that pre-render code */
interface IRunAnimationCallbackStart {
  startAnimation: (data: IRunAnimationCallbackRender) => void;
}

/** Callback that run rendering */
interface IRunAnimationCallbackRender {
  render: () => void;
}

interface IGestures {
  name: string;
  confidence: number;
}

interface IGestureMap {
  [prop: string]: TGestureMap;
}

const GestureMap: IGestureMap = {
  thumbs_up: 'ok',
  one_finger: 'one',
  victory: 'two',
  tree_fingers: 'tree',
  four_fingers: 'four',
  fist: 'fist',
  wave_hand_left: 'left',
  wave_hand_right: 'right',
  null: 'none'
};


type Gesture = 'one' | 'two' | 'tree' | 'four' | 'fist' | 'ok' | 'none';
type Direction = 'left' | 'right' | 'none';
export type TGestureMap = Gesture | Direction;
export type TGesturesIco = 'thumbs-up' | 'wave-hand' | 'fist' | 'one-finger' | 'two-fingers' | 'rock-fingers';
type Size = [number, number];
type Point = [number, number];
type Rect = { topLeft: [number, number]; bottomRight: [number, number] };

@Injectable({
  providedIn: 'root',
})
export class HandGesture {
  private _swipe$ = new BehaviorSubject<Direction>('none');
  readonly swipe$ = this._swipe$.asObservable();

  private _gesture$ = new BehaviorSubject<Gesture>('none');
  readonly gesture$ = this._gesture$.asObservable();

  private _initiated = false;
  private _initialTimestamp = -1;
  private _stream!: MediaStream;
  private _dimensions!: Size;
  private _lastGestureTiemstamp = -1;
  private _lastGesture: any = null;
  private _emitGesture = true;

  get stream(): (MediaStream | undefined) {
    return this._stream;
  }

  private _streamSettings!: MediaTrackSettings;
  get streamSettings(): MediaTrackSettings | undefined {
    return this._streamSettings;
  }

  constructor(private questionnaireActions: QuestionnaireActions) {
  }

  initialize(canvas: HTMLCanvasElement, video: HTMLVideoElement): void {
    this.questionnaireActions.loadingWebcam();
    this._dimensions = [video.width, video.height];
    navigator.mediaDevices
      .getUserMedia({video: true})
      .then((stream) => {
        this._stream = stream;
        this._streamSettings = stream.getVideoTracks()[0].getSettings();
        return handpose.load();
      })
      .then((model) => {
        const context: CanvasRenderingContext2D = canvas.getContext('2d');
        context.clearRect(0, 0, video.width, video.height);
        context.strokeStyle = 'red';
        context.fillStyle = 'red';
        context.translate(canvas.width, 0);
        context.scale(-1, 1); // made mirrored

        // the image is rendered at the frequency of the webcam
        this.runAnimation((data) => {
          model.estimateHands(video).then((predictions) => {
            data.startAnimation({
              render: () => { // Render
                context.drawImage(
                  video,
                  0,
                  0,
                  video.width,
                  video.height,
                  0,
                  0,
                  canvas.width,
                  canvas.height
                );
                if (predictions && predictions[0]) {
                  drawKeypoints(context, predictions[0].landmarks);
                  this._process(predictions[0].boundingBox);
                  this._processGesture(predictions[0].landmarks);
                }
              }
            });
          });
        }, this._streamSettings.frameRate);
      })
      .catch((err) => {
        this.questionnaireActions.errorLoadedWebcam(err);
        console.error(err);
      });
  }

  private _processGesture(landmarks: Coords3D): void {
    const {gestures} = GE.estimate(landmarks, 7.5) || [];
    const gesturesArray = (Array.prototype.slice.call(gestures, 0) as Array<IGestures>);
    let gesture = null;
    if (gesturesArray.length > 0) {
      const gesturesNames = gesturesArray.map(x => x.name);
      if (!gesture) {
        gesture = gesturesNames.find(x => x === 'thumbs_up');
      }
      if (!gesture) {
        gesture = gesturesNames.find(x => x === 'four_fingers');
      }
      if (!gesture) {
        gesture = gesturesNames.find(x => x === 'three_fingers');
      }
      if (!gesture) {
        gesture = gesturesNames.find(x => x === 'victory');
      }
      if (!gesture) {
        gesture = gesturesNames.find(x => x === 'one_finger');
      }
      if (!gesture) {
        gesture = gesturesNames.find(x => x === 'fist');
      }
    }
    if (this._lastGesture !== gesture) {
      this._lastGesture = gesture;
      this._lastGestureTiemstamp = Date.now();
      this._emitGesture = true;
    } else {
      if (
        this._emitGesture &&
        this._toSeconds(Date.now() - this._lastGestureTiemstamp) > 1
      ) {
        this._gesture$.next(GestureMap[this._lastGesture] as Gesture);
        this.questionnaireActions.changeGesture(GestureMap[this._lastGesture]);
        this._emitGesture = false;
      }
    }
  }

  private _process(rect: Rect): void {
    const middle = this._getMiddle(rect);
    if (this._aroundCenter(middle)) {
      this._initialTimestamp = Date.now();
      this._initiated = true;
      return;
    }
    if (!this._initiated) {
      return;
    }
    if (
      this._inRegion(0, 0.1, middle) &&
      this._toSeconds(Date.now() - this._initialTimestamp) < 2
    ) {
      this._swipe$.next('right');
      this.questionnaireActions.changeGesture(GestureMap.wave_hand_right);
      this._initiated = false;
      return;
    }
    if (
      this._inRegion(0.9, 1, middle) &&
      this._toSeconds(Date.now() - this._initialTimestamp) < 2
    ) {
      this._swipe$.next('left');
      this.questionnaireActions.changeGesture(GestureMap.wave_hand_left);
      this._initiated = false;
      return;
    }
  }

  private _toSeconds(ms: number): number {
    return ms / 1000;
  }

  private _aroundCenter(center: Point): boolean {
    return this._inRegion(0.4, 0.6, center);
  }

  private _inRegion(start: number, end: number, point: Point): boolean {
    return (
      this._dimensions[0] * start < point[0] &&
      this._dimensions[0] * end > point[0]
    );
  }

  private _getMiddle(rect: Rect): Point {
    return [
      rect.topLeft[0] + (rect.topLeft[0] + rect.bottomRight[0]) / 2,
      rect.topLeft[1] + (rect.topLeft[1] + rect.bottomRight[1]) / 2,
    ];
  }

  /**
   * Method for triggering a rendering constrained animation
   * @param callback callback with callback
   * @param fps how many frames per second will be rendered
   */
  private runAnimation(callback: IRunAnimationCallback, fps: number = 60): void {
    const fpsInterval = 1000 / fps;
    let then = Date.now();
    let firstDrow = false;
    const animation = () => {
      setTimeout(() => {
        // tslint:disable-next-line:no-unused-expression
        (!firstDrow && this.questionnaireActions.successLoadedWebcam());
        callback({
          startAnimation: (data) => {
            requestAnimationFrame(animation);
            firstDrow = true;
            const now = Date.now();
            const elapsed = now - then;
            if (elapsed > fpsInterval) {
              then = now - (elapsed % fpsInterval);
              data.render();
            }
          }
        });
      }, 0);
    };
    animation();
  }

  convertGestureMapToIco(gesture: TGestureMap): TGesturesIco {
    switch (gesture) {
      case 'one':
        return 'one-finger';
      case 'two':
        return 'two-fingers';
      case 'fist':
        return 'fist';
      case 'ok':
        return 'thumbs-up';
      case 'right':
        return 'wave-hand';
      case 'left':
        return 'wave-hand';
      default:
        return 'wave-hand';
    }
  }
}

