export interface ILoadingStatus{
  loading: boolean;
  loaded: boolean;
}

export const LOADING_ERROR: ILoadingStatus = {
  loaded: false,
  loading: false
}

export const LOADING_IN_PROGRESS: ILoadingStatus = {
  loaded: false,
  loading: true
}

export const LOADING_SUCCESS: ILoadingStatus = {
  loaded: true,
  loading: false
}
