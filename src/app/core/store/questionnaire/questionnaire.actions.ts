import {Injectable} from '@angular/core';
import {Update} from '@ngrx/entity';
import {createAction, props, Store} from '@ngrx/store';
import {TGestureMap} from 'src/app/core/services/hand-gesture.service';
import {IQuestionnaire, IUserData} from './questionnaire.reducer';

export const loadQuestionnaire = createAction(
  '[Questionnaire] Loading questionnaires cards'
);

export const loadQuestionnaireSuccess = createAction(
  '[Questionnaire] Load questionnaires cards Success',
  props<{ questionnaire: Array<IQuestionnaire> }>()
);

export const loadQuestionnaireFailure = createAction(
  '[Questionnaire] Load questionnaires cards Failure',
  props<{ error: any }>()
);

export const loadWebcamQuestionnaire = createAction(
  '[Questionnaire] Loading webcam'
);

export const loadWebcamQuestionnaireSuccess = createAction(
  '[Questionnaire] Success loaded webcam'
);

export const loadWebcamQuestionnaireFailure = createAction(
  '[Questionnaire] Error loaded webcam',
  props<{ error: any }>()
);

export const changeGesture = createAction(
  '[Questionnaire] Changing gesture',
  props<{ gesture: TGestureMap }>()
);

export const changeNextPrevIndexOfAnswerCard = createAction(
  '[Questionnaire] Change Index Of Answer Card',
  props<{ next: boolean }>()
);

export const answerOnCurrentQuestion = createAction(
  '[Questionnaire] Answer On Current Question',
  props<{ value: string }>()
);

export const updateQuestionnaire = createAction(
  '[Questionnaire] Update Questionnaire',
  props<{ update: Update<IQuestionnaire> }>()
);

export const allAnswered = createAction(
  '[Questionnaire] All questions were answered',
  props<{ status: boolean }>()
);

export const sendAnswers = createAction(
  '[Questionnaire] Send answers to database'
);

export const sendAnswersSuccess = createAction(
  '[Questionnaire] Success sending answers to database',
  props<{ response: any }>()
);

export const sendAnswersFailure = createAction(
  '[Questionnaire] Error sending answers to database',
  props<{ error: any }>()
);

export const addUserData = createAction(
  '[UserData] Add user data',
  props<{ userData: IUserData }>()
);

@Injectable()
export class QuestionnaireActions {

  constructor(private store$: Store) {
  }

  /**
   * Loading a webcam
   */
  loadingWebcam(): void {
    this.store$.dispatch(loadWebcamQuestionnaire());
  }

  /**
   * Webcam loaded
   */
  successLoadedWebcam(): void {
    this.store$.dispatch(loadWebcamQuestionnaireSuccess());
  }

  /**
   * Error loading webcam
   * @param error Error
   */
  errorLoadedWebcam(error: any): void {
    this.store$.dispatch(loadWebcamQuestionnaireFailure({error}));
  }

  /**
   * Loading questions
   */
  loadingQuestionnaires(): void {
    this.store$.dispatch(loadQuestionnaire());
  }

  /**
   * Uploaded questions
   * @param questionnaire list of questions
   */
  successLoadedQuestionnaires(questionnaire: Array<IQuestionnaire>): void {
    this.store$.dispatch(loadQuestionnaireSuccess({questionnaire}));
  }

  /**
   * Error loading questions
   * @param error Error
   */
  errorLoadeQuestionnaires(error: any): void {
    this.store$.dispatch(loadQuestionnaireFailure({error}));
  }

  /**
   * Change of gesture
   * @param gesture gesture
   */
  changeGesture(gesture: TGestureMap): void {
    this.store$.dispatch(changeGesture({gesture}));
  }

  /**
   * Change Index Of Answer Card
   * @param next next
   */
  changeNextPrevIndexOfAnswerCard(next: boolean): void {
    this.store$.dispatch(changeNextPrevIndexOfAnswerCard({next}));
  }

  /**
   * Update questionnaire
   * @param answer answer
   */
  updateQuestionnaire(update: Update<IQuestionnaire>): void {
    this.store$.dispatch(updateQuestionnaire({update}));
  }

  /**
   * Answer On Current Question
   * @param value answer
   */
  answerOnCurrentQuestion(value: string): void {
    this.store$.dispatch(answerOnCurrentQuestion({value}));
  }


  /**
   * All questions were answered
   * @param status status
   */
  allAnswered(status: boolean): void {
    this.store$.dispatch(allAnswered({status}));
  }

  /**
   * Send answers to database
   */
  sendAnswers(): void {
    this.store$.dispatch(sendAnswers());
  }

  /**
   * Success sending answers to database
   * @param response status
   */
  sendAnswersSuccess(response: any): void {
    this.store$.dispatch(sendAnswersSuccess({response}));
  }

  /**
   * Error sending answers to database
   * @param error status
   */
  sendAnswersFailure(error: any): void {
    this.store$.dispatch(sendAnswersFailure({error}));
  }

  addUserData(userData: any): void {
    this.store$.dispatch(addUserData({userData}));
  }
}

