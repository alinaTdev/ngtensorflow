import {Injectable} from '@angular/core';
import {Actions, concatLatestFrom, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import {DatabaseService, IRespondentAnswer} from 'src/app/core/services/database.service';
import * as actions from './questionnaire.actions';
import {QuestionnaireSelectors} from './questionnaire.selectors';


@Injectable()
export class QuestionnaireEffects {

  /**
   * Get list of questionnaire from database,
   *
   * if have response then emit actions of success loading data and transferring data in it
   */
  loadQuestionnaire$ = createEffect(() => this.actions$.pipe(
    ofType(actions.loadQuestionnaire),
    mergeMap(() => this.databaseService.getQuestionnaireCards().pipe(
      map(questionnaire => actions.loadQuestionnaireSuccess({questionnaire})),
      catchError((error) => of(actions.loadQuestionnaireFailure({error})))
    ))
  ));

  /**
   * Getting total of two entities
   *
   * if they are the same, then emit action allAnswered
   */
  allAnswered$ = createEffect(() => this.actions$.pipe(
    ofType(actions.updateQuestionnaire, actions.answerOnCurrentQuestion),
    concatLatestFrom(action => [
      this.questionnaireSelectors.getQuestionnaireAll(),
    ]),
    map(([, questionnaires]) => actions.allAnswered({status: questionnaires.every(data => typeof data.answer === 'string')}))
  ));

  /**
   * Send answers to database
   */
  sendAnswers$ = createEffect(() => this.actions$.pipe(
    ofType(actions.sendAnswers),
    concatLatestFrom(action => [
      this.questionnaireSelectors.getQuestionnaireAll(),
      this.questionnaireSelectors.getUserData(),
    ]),
    switchMap(([, answers, userData]) => {
        const RESPONDENT_ANSWERS: Array<IRespondentAnswer> = answers.map(data => {
          return ({question_text: data.text, answer: data.answer});
        });
        const DB_DATA = {
          respondent_answers: RESPONDENT_ANSWERS,
          respondent_first_name: userData.firstName,
          respondent_last_name: userData.lastName,
          respondent_age: userData.age,
        };

        return this.databaseService
          .sendAnswersQuestionnaire(DB_DATA);
      }
    ),
    map((response) => {
      return actions.sendAnswersSuccess({response});
    }),
    catchError((error) => of(actions.sendAnswersFailure({error})))
  ));

  constructor(
    private actions$: Actions,
    private questionnaireSelectors: QuestionnaireSelectors,
    private databaseService: DatabaseService
  ) {
  }
}
