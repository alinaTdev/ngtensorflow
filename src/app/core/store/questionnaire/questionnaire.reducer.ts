import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {Action, createReducer, on} from '@ngrx/store';
import {TGestureMap} from 'src/app/core/services/hand-gesture.service';
import {ILoadingStatus, LOADING_ERROR, LOADING_IN_PROGRESS, LOADING_SUCCESS} from '../loading';
import * as actions from './questionnaire.actions';


export const questionnaireFeatureKey = 'questionnaire';

export interface IQuestionnaire {
  id: string;
  image: string;
  text: string;
  answer?: string;
}

interface ICurrentGesture {
  value: TGestureMap;
  updateDate: number;
}

interface ISend<T, E> {
  start: boolean;
  response: boolean;
  responseData?: T;
  error?: E;
}

export interface IUserData {
  firstName: string;
  lastName: string;
  age: string;
}

export interface State extends EntityState<IQuestionnaire> {
  webCamLoading: ILoadingStatus;
  currentGesture: ICurrentGesture;
  questionnaireLoading: ILoadingStatus;
  indexOfCurrentCard: number;
  allAnswered: boolean;
  sendAnswer: ISend<any, any>;
  userData: IUserData;
  userDataFilledIn: boolean;
}

export const questionnaireAdapter = createEntityAdapter<IQuestionnaire>();


export const initialState: State = questionnaireAdapter.getInitialState({
  webCamLoading: LOADING_ERROR,
  questionnaireLoading: LOADING_ERROR,
  currentGesture: {
    value: 'none',
    updateDate: Date.now()
  },
  indexOfCurrentCard: 0,
  allAnswered: false,
  sendAnswer: {
    start: false,
    response: false
  },
  userData: {
    firstName: null,
    lastName: null,
    age: null
  },
  userDataFilledIn: false,
});


const questionnaireReducer = createReducer(
  initialState,
  on(actions.loadWebcamQuestionnaire, (state) => ({
    ...state,
    webCamLoading: LOADING_IN_PROGRESS
  })),
  on(actions.loadWebcamQuestionnaireSuccess, (state) => ({
    ...state,
    webCamLoading: LOADING_SUCCESS
  })),
  on(actions.loadWebcamQuestionnaireFailure, (state) => ({
    ...state,
    webCamLoading: LOADING_ERROR
  })),
  on(actions.changeGesture, (state, {gesture}) => ({
    ...state,
    currentGesture: {
      value: gesture,
      updateDate: Date.now() // add a date to trigger the action when re-specifying the gesture
    }
  })),
  on(actions.loadQuestionnaire, (state) => ({
    ...state,
    questionnaireLoading: LOADING_IN_PROGRESS
  })),
  on(actions.loadQuestionnaireSuccess, (state, {questionnaire}) => {
    return {
      ...questionnaireAdapter.addMany(questionnaire, state),
      questionnaireLoading: LOADING_SUCCESS
    };
  }),
  on(actions.loadQuestionnaireFailure, (state) => ({
    ...state,
    questionnaireLoading: LOADING_ERROR
  })),
  on(actions.changeNextPrevIndexOfAnswerCard, (state, {next}) => {
    let nextIndex: number = state.indexOfCurrentCard + (next ? +1 : -1);
    if (nextIndex < 0 || nextIndex === state.ids.length) {
      nextIndex = state.indexOfCurrentCard;
    }
    return {
      ...state,
      indexOfCurrentCard: nextIndex
    };
  }),
  on(actions.updateQuestionnaire, (state, {update}) => {
    return {
      ...questionnaireAdapter.updateOne(update, state)
    };
  }),
  on(actions.answerOnCurrentQuestion, (state, {value}) => {
    const currentQuestionnaire = selectAll(state)[state.indexOfCurrentCard];
    return {
      ...questionnaireAdapter.updateOne({
        id: currentQuestionnaire.id,
        changes: {
          answer: value
        }
      }, state)
    };
  }),
  on(actions.allAnswered, (state, {status}) => ({
    ...state,
    allAnswered: status
  })),
  on(actions.sendAnswers, (state) => ({
    ...state,
    sendAnswer: {
      start: true,
      response: false
    }
  })),
  on(actions.sendAnswersSuccess, (state, {response}) => ({
    ...state,
    sendAnswer: {
      start: false,
      response: true,
      responseData: response
    }
  })),
  on(actions.sendAnswersFailure, (state, {error}) => ({
    ...state,
    sendAnswer: {
      start: false,
      response: false,
      error
    }
  })),
  on(actions.addUserData, (state, {userData}) => ({
    ...state,
    userData: {
      firstName: userData.firstName,
      lastName: userData.lastName,
      age: userData.age,
    },
    userDataFilledIn: true,
  })),
);

// tslint:disable-next-line:typedef
export function reducer(state: State | undefined, action: Action) {
  return questionnaireReducer(state, action);
}

const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = questionnaireAdapter.getSelectors();

export const selectQuestionnaireIds = selectIds;

export const selectQuestionnaireEntities = selectEntities;

export const selectAllQuestionnaire = selectAll;

export const selectQuestionnaireTotal = selectTotal;

