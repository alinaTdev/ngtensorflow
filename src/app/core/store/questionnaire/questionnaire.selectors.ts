import {Injectable} from '@angular/core';
import {ActionReducerMap, createFeatureSelector, createSelector, Store} from '@ngrx/store';
import {map, switchMap} from 'rxjs/operators';

import * as reducer from './questionnaire.reducer';

export interface State {
  products: reducer.State;
}

export const reducers: ActionReducerMap<State> = {
  products: reducer.reducer,
};

export const selectQuestionnaireFeatureState = createFeatureSelector<reducer.State>(reducer.questionnaireFeatureKey);

export const selectQuestionnaireTotal =
  createSelector(selectQuestionnaireFeatureState, reducer.selectQuestionnaireTotal);

export const selectQuestionnaireEntities =
  createSelector(selectQuestionnaireFeatureState, reducer.selectQuestionnaireEntities);

export const selectQuestionnaireAll =
  createSelector(selectQuestionnaireFeatureState, reducer.selectAllQuestionnaire);

export const selectQuestionnaireIds =
  createSelector(selectQuestionnaireFeatureState, reducer.selectQuestionnaireIds);

export const selectQuestionnaireWebcamLoadingStatus =
  createSelector(selectQuestionnaireFeatureState, (state) => state.webCamLoading.loading);

export const selectQuestionnaireWebcamLoadedStatus =
  createSelector(selectQuestionnaireFeatureState, (state) => state.webCamLoading.loaded);

export const selectQuestionnaireLoadingStatus =
  createSelector(selectQuestionnaireFeatureState, (state) => state.questionnaireLoading.loading);

export const selectQuestionnaireLoadedStatus =
  createSelector(selectQuestionnaireFeatureState, (state) => state.questionnaireLoading.loaded);

export const selectQuestionnaireCurrentGesture =
  createSelector(selectQuestionnaireFeatureState, (state) => state.currentGesture);

export const selectQuestionnaireIndexOfAnswerCard =
  createSelector(selectQuestionnaireFeatureState, (state) => state.indexOfCurrentCard);

export const selectAllAnsweredQuestionnaire =
  createSelector(selectQuestionnaireFeatureState, (state) => state.allAnswered);

export const selectUserFilledDataStatus =
  createSelector(selectQuestionnaireFeatureState, (state) => state.userDataFilledIn);

export const selectUserData =
  createSelector(selectQuestionnaireFeatureState, (state) => state.userData);

@Injectable()
export class QuestionnaireSelectors {

  constructor(private store$: Store) {
  }

  /**
   * Get select of status starting loading webcam
   */
  // tslint:disable-next-line:typedef
  getLoadingWebcamStatus() {
    return this.store$.select(selectQuestionnaireWebcamLoadingStatus);
  }

  /**
   * Get select of status successfully loaded webcam
   */
  // tslint:disable-next-line:typedef
  getLoadedWebcamStatus() {
    return this.store$.select(selectQuestionnaireWebcamLoadedStatus);
  }


  /**
   * Get select of error status loding webcam
   */
  // tslint:disable-next-line:typedef
  getErrorLoadedWebcamStatus() {
    return this.getLoadingWebcamStatus().pipe(
      switchMap(loading =>
        this.getLoadedWebcamStatus().pipe(
          map(loaded => loaded === loading)
        )
      )
    );
  }

  /**
   * Get select of current gesture
   */
  // tslint:disable-next-line:typedef
  getCurrentGesture() {
    return this.store$.select(selectQuestionnaireCurrentGesture);
  }

  /**
   * Get select of status starting loading questionnaire
   */
  // tslint:disable-next-line:typedef
  getLoadingQuestionnaireStatus() {
    return this.store$.select(selectQuestionnaireLoadingStatus);
  }

  /**
   * Get select of status successfully loaded questionnaire
   */
  // tslint:disable-next-line:typedef
  getLoadedQuestionnaireStatus() {
    return this.store$.select(selectQuestionnaireLoadedStatus);
  }


  /**
   * Get select of error status loding questionnaire
   */
  // tslint:disable-next-line:typedef
  getErrorLoadedQuestionnaireStatus() {
    return this.getLoadingWebcamStatus().pipe(
      switchMap(loading =>
        this.getLoadedWebcamStatus().pipe(
          map(loaded => loaded === loading)
        )
      )
    );
  }

  /**
   * Get select of all questions in questionnaire
   */
  // tslint:disable-next-line:typedef
  getQuestionnaireAll() {
    return this.store$.select(selectQuestionnaireAll);
  }

  // tslint:disable-next-line:typedef
  getUserData() {
    return this.store$.select(selectUserData);
  }

  /**
   * Get select of total questions in questionnaire
   */
  // tslint:disable-next-line:typedef
  getQuestionnaireTotal() {
    return this.store$.select(selectQuestionnaireTotal);
  }

  /**
   * Get select of index of answer card
   */
  // tslint:disable-next-line:typedef
  getIndexOfAnswerCard() {
    return this.store$.select(selectQuestionnaireIndexOfAnswerCard);
  }

  /**
   * Get select of same gesture
   */
  // tslint:disable-next-line:typedef
  getSameGesture() {
    return this.store$.select(selectQuestionnaireCurrentGesture);
  }

  // tslint:disable-next-line:typedef
  getAllAnsweredStatus() {
    return this.store$.select(selectAllAnsweredQuestionnaire);
  }

  // tslint:disable-next-line:typedef
  getUserFilledDataStatus() {
    return this.store$.select(selectUserFilledDataStatus);
  }
}
