import * as fingerpose from 'fingerpose';

//#region oneFinger

const oneFingerGesture = new fingerpose.GestureDescription('one_finger');
// thumb:
oneFingerGesture.addCurl(fingerpose.Finger.Thumb, fingerpose.FingerCurl.HalfCurl, 0.5);
oneFingerGesture.addCurl(fingerpose.Finger.Thumb, fingerpose.FingerCurl.FullCurl, 0.5);
oneFingerGesture.addDirection(fingerpose.Finger.Thumb, fingerpose.FingerDirection.VerticalUp, 0.2);
oneFingerGesture.addDirection(fingerpose.Finger.Thumb, fingerpose.FingerDirection.DiagonalUpLeft, 1.0);

// index:
oneFingerGesture.addCurl(fingerpose.Finger.Index, fingerpose.FingerCurl.NoCurl, 1.0);
oneFingerGesture.addDirection(fingerpose.Finger.Index, fingerpose.FingerDirection.VerticalUp, 0.8);
oneFingerGesture.addDirection(fingerpose.Finger.Index, fingerpose.FingerDirection.DiagonalUpLeft, 0.2);
oneFingerGesture.addDirection(fingerpose.Finger.Index, fingerpose.FingerDirection.DiagonalUpRight, 0.2);

for (const finger of [fingerpose.Finger.Middle, fingerpose.Finger.Ring, fingerpose.Finger.Pinky]) {
  oneFingerGesture.addCurl(finger, fingerpose.FingerCurl.FullCurl, 1.0);
  oneFingerGesture.addDirection(finger, fingerpose.FingerDirection.VerticalUp, 1.0);
  oneFingerGesture.addDirection(finger, fingerpose.FingerDirection.DiagonalUpLeft, 1.0);
}
// give additional weight to index and ring fingers
oneFingerGesture.setWeight(fingerpose.Finger.Index, 2);

//#endregion

//#region fist

const fistGesture = new fingerpose.GestureDescription('fist');

fistGesture.addCurl(fingerpose.Finger.Thumb, fingerpose.FingerCurl.FullCurl, 0.5);
fistGesture.addCurl(fingerpose.Finger.Thumb, fingerpose.FingerCurl.HalfCurl, 0.5);

for (const finger of [fingerpose.Finger.Index, fingerpose.Finger.Middle, fingerpose.Finger.Ring, fingerpose.Finger.Pinky]) {
  fistGesture.addCurl(finger, fingerpose.FingerCurl.FullCurl, 1.0);
  fistGesture.addDirection(finger, fingerpose.FingerDirection.VerticalUp, 1.0);
  fistGesture.addDirection(finger, fingerpose.FingerDirection.DiagonalUpLeft, 1.0);
}

//#endregion

//#region threeFingers it works incorrectly

const threeFingersGesture = new fingerpose.GestureDescription('three_fingers');
threeFingersGesture.addCurl(
  fingerpose.Finger.Index,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
threeFingersGesture.addCurl(
  fingerpose.Finger.Thumb,
  fingerpose.FingerCurl.FullCurl,
  1.0
);
threeFingersGesture.addCurl(
  fingerpose.Finger.Middle,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
threeFingersGesture.addCurl(
  fingerpose.Finger.Ring,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
threeFingersGesture.addCurl(
  fingerpose.Finger.Pinky,
  fingerpose.FingerCurl.FullCurl,
  1.0
);

//#endregion

//#region fourFingers it works incorrectly

const fourFingersGesture = new fingerpose.GestureDescription('four_fingers');
fourFingersGesture.addCurl(
  fingerpose.Finger.Index,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
fourFingersGesture.addCurl(
  fingerpose.Finger.Thumb,
  fingerpose.FingerCurl.FullCurl,
  1.0
);
fourFingersGesture.addCurl(
  fingerpose.Finger.Middle,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
fourFingersGesture.addCurl(
  fingerpose.Finger.Ring,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
fourFingersGesture.addCurl(
  fingerpose.Finger.Pinky,
  fingerpose.FingerCurl.NoCurl,
  1.0
);

//#endregion

//#region rockFingers it works incorrectly

const rockFingersGesture = new fingerpose.GestureDescription('rock_fingers');
rockFingersGesture.addCurl(
  fingerpose.Finger.Index,
  fingerpose.FingerCurl.NoCurl,
  1.0
);
rockFingersGesture.addCurl(
  fingerpose.Finger.Thumb,
  fingerpose.FingerCurl.FullCurl,
  1.0
);
rockFingersGesture.addCurl(
  fingerpose.Finger.Middle,
  fingerpose.FingerCurl.FullCurl,
  1.0
);
rockFingersGesture.addCurl(
  fingerpose.Finger.Ring,
  fingerpose.FingerCurl.FullCurl,
  1.0
);
rockFingersGesture.addCurl(
  fingerpose.Finger.Pinky,
  fingerpose.FingerCurl.NoCurl,
  1.0
);

//#endregion

export const GE = new fingerpose.GestureEstimator([
  fingerpose.Gestures.VictoryGesture,
  fingerpose.Gestures.ThumbsUpGesture,
  oneFingerGesture,
  fistGesture,
  // threeFingersGesture,
  // fourFingersGesture,
  // rockFingersGesture
]);
