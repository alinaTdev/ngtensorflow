import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {

  @Output() answer = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {
  }

  selectedAnswer(value: boolean): void {
    this.answer.emit(value);
  }
}
