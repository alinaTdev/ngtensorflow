import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {QuestionnaireActions} from 'src/app/core/store/questionnaire/questionnaire.actions';
import {IQuestionnaire} from '../../../core/store/questionnaire/questionnaire.reducer';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() data: IQuestionnaire;
  @Output() getIdQuestionnaire = new EventEmitter<IQuestionnaire['id']>();

  private _current = 0;
  @Input()
  public set current(v: number) {
    this._current = v;
  }

  public get current(): number {
    return this._current + 1;
  }

  @Input() total = 1;

  public get text(): string {
    return this.data.text;
  }

  public get imageSrc(): string {
    return this.data.image;
  }

  public get imageAlt(): string {
    return 'image';
  }

  constructor(private actionsQuestionnairesService: QuestionnaireActions) {
  }

  ngOnInit(): void {
    this.getIdQuestionnaire.emit(this.data.id);
  }

  answeredToTheQuestionnaire(value: boolean): void {
    this.actionsQuestionnairesService.updateQuestionnaire({
      id: this.data.id,
      changes: {
        answer: value ? 'yes' : 'no'
      }
    });
  }
}
