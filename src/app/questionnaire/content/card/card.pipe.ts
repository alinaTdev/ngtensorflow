import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'count'
})
export class CardPipe implements PipeTransform {

  transform(value: number, size: number = 10): string {
    if (size >= 10 && size < 100) {
      return this.ten(value);
    }
    if (size >= 100 ) {
      return this.hundred(value);
    }
    return value.toString();
  }

  private ten(value: number) {
    return value < 10 ? '0' + value.toString(): value.toString();
  }

  private hundred(value: number) {
    let toTen: string = this.ten(value);
    return value < 100 ? '0' + toTen: toTen;
  }

}
