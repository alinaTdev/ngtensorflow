import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {questionnaireAnimations} from '../questionnaire.animation';
import {QuestionnaireSelectors} from '../../core/store/questionnaire/questionnaire.selectors';
import {QuestionnaireActions} from '../../core/store/questionnaire/questionnaire.actions';
import {Router} from '@angular/router';
import {filter, take} from 'rxjs/operators';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  animations: [
    questionnaireAnimations.slideCardToLeft,
    questionnaireAnimations.skipInitialAnimation,
    questionnaireAnimations.slideCardTwoWay
  ]
})
export class ContentComponent implements OnInit, OnDestroy {
  currentIndexOfCard = 0;
  private _currentIdQuestionnaire: string;

  public set currentIdQuestionnaire(id: string) {
    this._currentIdQuestionnaire = id;
  }

  public get currentIdQuestionnaire(): string {
    return this._currentIdQuestionnaire;
  }

  buttonAnswer: boolean | string;
  private totalQuestionnaire: number;
  listQuestionnaire$ = this.questionnaireSelectors.getQuestionnaireAll();
  questionnaireTotal$ = this.questionnaireSelectors.getQuestionnaireTotal().pipe(filter(total => !!total), take(1));
  questionnaireLoaded$ = this.questionnaireSelectors.getLoadedQuestionnaireStatus();
  currentGesture$ = this.questionnaireSelectors.getCurrentGesture();
  currentIndexOfAnswerCard$ = this.questionnaireSelectors.getIndexOfAnswerCard();
  subscriptionCurrentGesture$: Subscription;
  subscriptionCurrentIndexOfAnswerCard$: Subscription;

  constructor(
    private questionnaireSelectors: QuestionnaireSelectors,
    private questionnaireActions: QuestionnaireActions,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.questionnaireActions.loadingQuestionnaires();
    this.questionnaireTotal$.subscribe((total) => this.totalQuestionnaire = total);
    this.subscriptionCurrentIndexOfAnswerCard$ =
      this.currentIndexOfAnswerCard$
        .subscribe((current) => this.currentIndexOfCard = current);
    this.subscriptionCurrentGesture$ = this.currentGesture$.subscribe((gesture) => {
      switch (gesture.value) {
        case 'ok':
          this.selectedAnswer(true);
          break;
        case 'fist':
          this.selectedAnswer(false);
          break;
        case 'left':
          this.selectedAnswer('left');
          break;

        case 'right':
          this.selectedAnswer('right');
          break;
        default:
          break;
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptionCurrentGesture$.unsubscribe();
    this.subscriptionCurrentIndexOfAnswerCard$.unsubscribe();
  }

  selectedAnswer(value: boolean | string): void {
    this.buttonAnswer = value;
    const answer = value === true ? 'yes' : value === false ? 'no' : 'skipped';
    this.questionnaireActions.answerOnCurrentQuestion(answer);
    if (this.currentIndexOfCard !== this.totalQuestionnaire - 1) {
      if (value === 'right') {
        this.questionnaireActions.changeNextPrevIndexOfAnswerCard(false);
      } else {
        this.questionnaireActions.changeNextPrevIndexOfAnswerCard(true);
      }
    } else {
      this.questionnaireActions.sendAnswers();
      this.router.navigate(['/answers']);
    }
  }

}
