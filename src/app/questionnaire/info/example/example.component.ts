import {Component, OnInit} from '@angular/core';
import {questionnaireAnimations} from '../../questionnaire.animation';
import {IGesturesExample} from '../../../core/interfaces/gesture';
import {GESTURE_LIST} from '../../../core/constants/gesture-list';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss'],
  animations: [
    questionnaireAnimations.opacity
  ]
})
export class ExampleComponent implements OnInit {

  isOpened = false;
  selectedGesture: IGesturesExample = GESTURE_LIST[0];

  constructor() {
  }

  ngOnInit(): void {
  }

  toggleStatus(): void {
    this.isOpened = !this.isOpened;
  }

  changeSelectedGesture(selected): void {
    this.selectedGesture = selected;
    this.toggleStatus();
  }
}
