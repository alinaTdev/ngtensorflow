import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GESTURE_LIST} from '../../../../core/constants/gesture-list';
import {IGesturesExample} from '../../../../core/interfaces/gesture';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Output() selectedGesture = new EventEmitter<IGesturesExample>();
  public gestureList = GESTURE_LIST;

  constructor() {
  }

  ngOnInit(): void {
  }
}
