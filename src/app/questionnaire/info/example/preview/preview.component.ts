import {Component, Input, OnInit} from '@angular/core';
import {IGesturesExample} from '../../../../core/interfaces/gesture';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  @Input() selectedGesture: IGesturesExample;

  constructor() {
  }

  ngOnInit(): void {
  }

}
