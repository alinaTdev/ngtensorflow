import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {map} from 'rxjs/operators';
import {HandGesture} from 'src/app/core/services/hand-gesture.service';
import {QuestionnaireSelectors} from '../../core/store/questionnaire/questionnaire.selectors';
import {questionnaireAnimations} from '../questionnaire.animation';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  animations: [
    questionnaireAnimations.opacity
  ]
})
export class InfoComponent implements OnInit, AfterViewInit {

  @ViewChild('video')
  video!: ElementRef<HTMLVideoElement>;

  @ViewChild('canvas')
  canvas!: ElementRef<HTMLCanvasElement>;

  loadedWebcam$ = this.questionnaireSelectors.getLoadedWebcamStatus();

  currentGestureIco$ =
    this.questionnaireSelectors.getCurrentGesture()
      .pipe(map(x => this._recognizer.convertGestureMapToIco(x.value)));

  get stream(): MediaStream | undefined {
    return this._recognizer.stream;
  }

  get streamWidth(): number {
    return this._recognizer.streamSettings?.width ?? 550;
  }

  get streamHeight(): number {
    return this._recognizer.streamSettings?.height ?? 400;
  }

  constructor(private _recognizer: HandGesture, private questionnaireSelectors: QuestionnaireSelectors) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this._recognizer.initialize(
      this.canvas.nativeElement,
      this.video.nativeElement
    );
  }
}




