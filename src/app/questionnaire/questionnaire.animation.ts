import { animate, AnimationMetadata, AnimationTriggerMetadata, group, query, stagger, style, transition, trigger } from '@angular/animations';

const opacityAnimate: string = '200ms';
const slideCardAnimate: string = '300ms ease-in';
export const questionnaireAnimations: {
  readonly opacity: AnimationTriggerMetadata;
  /** skip initial animation need add on parent element of target of skipping */
  readonly skipInitialAnimation: AnimationTriggerMetadata;
  readonly slideCardToRight: AnimationTriggerMetadata;
  readonly slideCardToLeft: AnimationTriggerMetadata;
  readonly slideCardTwoWay: AnimationTriggerMetadata;
} = {
  opacity: trigger('opacityAnimations', [
    transition(':enter', [
      style({
        opacity: '0',
        position: 'absolute'
      }),
      animate(opacityAnimate)
    ]),
    transition(':leave', [
      animate(opacityAnimate, style({
        opacity: '0',
        position: 'absolute'
      }))
    ])
  ]),
  slideCardToRight: trigger('slideCardToRight', slideCard(false)),
  slideCardToLeft: trigger('slideCardToLeft', slideCard(true)),
  slideCardTwoWay: trigger('slideCardTwoWay', [
    transition(':enter, * => -1', []),
    transition(':increment', slideCardQuery(true)),
    transition(':decrement', slideCardQuery(false)),
  ]),
  skipInitialAnimation: trigger('skipInitialAnimation', [
    transition(':enter', [])
  ])
}

function slideCard(toLeft: boolean): Array<AnimationMetadata> {
  const enter: string = toLeft ? '' : '-';
  const leave: string = !toLeft ? '' : '-';
  return [
    transition(':enter', [
      style({
        top: '0',
        left: '0',
        width: '100%',
        position: 'absolute',
        opacity: '0',
        transform: `translateX(${enter}200%) translateY(10%) rotate(${enter}8.11deg)`
      }),
      group([
        animate(slideCardAnimate, style({
          transform: 'translateX(0%) translateY(0%) rotate(0deg)'
        })),
        animate(slideCardAnimate, style({
          opacity: '1'
        }))
      ])
    ]),
    transition(':leave', [
      group([
        animate(slideCardAnimate, style({
          top: '0',
          left: '0',
          width: '100%',
          position: 'absolute',
          transform: `translateX(${leave}200%) translateY(10%) rotate(${leave}8.11deg)`
        })),
        animate(slideCardAnimate, style({
          opacity: 0
        }))
      ])
    ])
  ]
}

function slideCardQuery(toLeft: boolean) {
  const enter: string = toLeft ? '' : '-';
  const leave: string = !toLeft ? '' : '-';
  return [
    query(':enter, :leave', [
      style({
        top: '0',
        left: '0',
        width: '100%',
        position: 'absolute',
        opacity: '1',
        zIndex: '10',
        transform: `translateX(0%) translateY(0%) rotate(0deg)`
      })
    ], { optional: true }),
    query(':enter', [
      style({
        opacity: '0',
        transform: `translateX(${enter}200%) translateY(10%) rotate(${enter}8.11deg)`
      })
    ]),
    group([
      query(':leave', [
        animate(slideCardAnimate, style({
          opacity: 0,
          transform: `translateX(${leave}200%) translateY(10%) rotate(${leave}8.11deg)`
        }))
      ], { optional: true }),
      query(':enter', [
        animate(slideCardAnimate, style({
          opacity: 1,
          transform: 'translateX(0%) translateY(0%) rotate(0deg)'
        }))
      ])
    ])

  ];
}
