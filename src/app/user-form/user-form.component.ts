import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {QuestionnaireActions} from '../core/store/questionnaire/questionnaire.actions';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  public userFormGroup = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    age: new FormControl('',
      [Validators.required, Validators.pattern('^[1-9][0-9]*$')]),
  });

  constructor(
    private questionnaireActions: QuestionnaireActions,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmitUserForm(): void {
    this.questionnaireActions.addUserData(this.userFormGroup.value);
    this.router.navigateByUrl('/questionnaire');
  }
}
