// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCT0tbLcwPzI32CjMSeqrvHQVnQSl6k_7Q',
    authDomain: 'angular-tensorflow-79ed4.firebaseapp.com',
    projectId: 'angular-tensorflow-79ed4',
    storageBucket: 'angular-tensorflow-79ed4.appspot.com',
    messagingSenderId: '921890580008',
    appId: '1:921890580008:web:fac330d4fc32aedf96b71d',
    measurementId: 'G-DY68906VV1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
